#include "Include/BearLibTerminal.h"

#include <string.h>

namespace Rendering {
class Renderer {
    public:
    Renderer() {};
    virtual void print(int x, int y, char* string) {};
    virtual void refresh() {};
    virtual void update() {};
    virtual bool should_close() { return true; };
    virtual void print_centered(int y, char* string) {};
    virtual void clear() {};
    ~Renderer() {};
};

struct RendererBLT : public Renderer {
    int m_last_read = TK_INPUT_NONE;

    public:
    RendererBLT() {
        terminal_open();
        terminal_refresh();
    }
    ~RendererBLT() {
        terminal_close();
    }

    void print(int x, int y, char* string) override {
        terminal_print(x, y, string);
    }
    void update() override {
        terminal_refresh();
        m_last_read = terminal_read();
    }
    void refresh() override {terminal_refresh(); }
    bool should_close() override { return m_last_read == TK_CLOSE;}

    void print_centered(int y, char* string) override {
        this->print((80-strlen(string))/2, y, string);
    }

    void clear() override { terminal_clear(); }

};
}

namespace UI {
    class Screen {
        public:
        virtual void draw(Rendering::Renderer* r) {}
        virtual void update(Rendering::Renderer* r) {};
    };

    class TitleScreen : public Screen {
        private:
        char* m_title;
        public:
        TitleScreen(char* title) : m_title(title) {}
        void draw(Rendering::Renderer* r) {
            r->clear();
            r->print_centered(1, m_title);
            
            r->print_centered(3, "a basic game engine");
        }
    };
}


int main() {
    Rendering::Renderer* r = new Rendering::RendererBLT();
    UI::Screen* s = new UI::TitleScreen("/test engine/");
    r->refresh();
    r->refresh();

    while(!r->should_close()) {
        s->draw(r);
        r->update();
    };

}