
game: src/main.cpp
	g++ src\main.cpp -I. -L./lib -lBearLibTerminal -o bin/game

run: game
	./bin/game
	
clean:
	rm bin/game*